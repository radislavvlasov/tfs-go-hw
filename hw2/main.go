package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

type News struct {
	ID        int64     `json:"id"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	Provider  string    `json:"provider"`
	Published time.Time `json:"published_at"`
	Tickers   []string  `json:"tickers"`
}

type Payload struct {
	Items     []News    `json:"items"`
	Published time.Time `json:"published_at"`
	Tickers   []string  `json:"tickers"`
}

type FeedItem struct {
	Type    string  `json:"type"`
	Payload Payload `json:"payload"`
}

type NewsMarshal struct {
	Type     string `json:"type"`
	PayloadN News   `json:"payload"`
}

func writeJSON(feed []FeedItem, outJSON *os.File) error {
	_, err := outJSON.Write([]byte("[\n    "))
	if err != nil {
		return nil
	}
	for item := range feed {
		var byteArray []byte
		if len(feed[item].Payload.Items) > 1 {
			byteArray, err = json.MarshalIndent(feed[item], "    ", "    ")
		} else {
			byteArray, err = json.MarshalIndent(NewsMarshal{
				Type:     feed[item].Type,
				PayloadN: feed[item].Payload.Items[0],
			}, "    ", "    ")
		}
		if err != nil {
			return err
		}
		if item != len(feed)-1 {
			_, err = outJSON.Write(append(byteArray, []byte(",\n    ")...))
			if err != nil {
				return err
			}
		} else {
			_, err = outJSON.Write(append(byteArray, []byte("\n")...))
			if err != nil {
				return err
			}
		}
	}
	_, err = outJSON.Write([]byte("]"))
	if err != nil {
		return nil
	}
	return nil
}

func equalTickers(tickers1, tickers2 []string) bool {
	if len(tickers1) != len(tickers2) {
		return false
	}
	var temp string
	for i := range tickers1 {
		temp += tickers1[i]
	}
	for i := range tickers2 {
		if !strings.Contains(temp, tickers2[i]) {
			return false
		}
	}
	return true
}

func containTickers(feed []FeedItem, news News) (bool, int) {
	for i := range feed {
		if equalTickers(feed[i].Payload.Tickers, news.Tickers) && feed[i].Payload.Published.YearDay() == news.Published.YearDay() {
			return true, i
		}
	}
	return false, 0
}

func groupNews(news []News) []FeedItem {
	var feed []FeedItem
	for i := range news {
		contain, number := containTickers(feed, news[i])
		if contain {
			feed[number].Payload.Items = append(feed[number].Payload.Items, news[i])
		} else {
			feed = append(feed, FeedItem{
				Type: "news",
				Payload: Payload{
					Items:     []News{news[i]},
					Published: news[i].Published,
					Tickers:   news[i].Tickers,
				},
			})
		}
	}
	for item := range feed {
		if len(feed[item].Payload.Items) != 1 {
			sort.Slice(feed[item].Payload.Items, func(j1, j2 int) bool {
				if feed[item].Payload.Items[j1].Published.After(feed[item].Payload.Items[j2].Published) {
					return false
				}
				return true
			})
			feed[item].Type = "company_news"
			feed[item].Payload.Published = feed[item].Payload.Items[0].Published
			feed[item].Payload.Tickers = feed[item].Payload.Items[0].Tickers
		}
	}
	sort.Slice(feed, func(i, j int) bool {
		if feed[i].Payload.Published.Equal(feed[j].Payload.Published) {
			if len(feed[i].Payload.Items) != 1 && len(feed[j].Payload.Items) == 1 {
				return true
			}
			return false
		}
		return feed[i].Payload.Published.Before(feed[j].Payload.Published)
	})
	return feed
}

func main() {
	path, err := filepath.Abs("")
	if err != nil {
		log.Fatalln(err)
	}
	byteArray, err := ioutil.ReadFile(os.Args[1])

	if err != nil {
		log.Fatalln(err)
	}
	var news []News
	err = json.Unmarshal(byteArray, &news)
	if err != nil {
		log.Fatalln(err)
	}

	feed := groupNews(news)

	outJSON, err := os.Create(filepath.Join(path, "out.json"))
	if err != nil {
		log.Fatalln(err)
	}

	err = writeJSON(feed, outJSON)
	if err != nil {
		log.Fatalln(err)
	}
	err = outJSON.Close()
	if err != nil {
		log.Fatalln(err)
	}
}
