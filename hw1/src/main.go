package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const timeLayout = "2006-01-02 15:04:05"

type Candle struct {
	opened    bool
	open      float64
	high      float64
	low       float64
	close     float64
	period    time.Duration
	timeOpen  time.Time
	timeClose time.Time
}

func initSlices(openTime, closeTime time.Time) [3][3][]Candle {
	var candles [3][3][]Candle
	periods := []int{5, 30, 240}
	for period := 0; period < 3; period++ {
		for ticker := 0; ticker < 3; ticker++ {
			openTime1 := openTime
			for openTime1.Before(closeTime) {
				candles[period][ticker] = append(candles[period][ticker], Candle{
					opened:    false,
					open:      0,
					high:      0,
					low:       0,
					close:     0,
					period:    time.Minute * time.Duration(periods[period]),
					timeOpen:  openTime1,
					timeClose: openTime1.Add(time.Minute*time.Duration(periods[period])),
				})
				openTime1 = openTime1.Add(time.Minute*time.Duration(periods[period]))
			}
		}
	}
	return candles
}

func maxSliceLength(candles [3][]Candle) int {
	max := -1
	for i := range candles {
		if max < len(candles[i]) {
			max = len(candles[i])
		}
	}
	return max
}

func writeSlices(candles [3][3][]Candle, writers [3]csv.Writer) {
	var strings [3][3][300][]string
	tickerStr := []string{"SBER", "AAPL", "AMZN"}
	for period := range candles {
		for ticker := range candles[period] {
			for i := range candles[period][ticker]{
				if !candles[period][ticker][i].opened {
					continue
				}
				str := []string{tickerStr[ticker], fmt.Sprint(candles[period][ticker][i].timeOpen.Format(time.RFC3339)), fmt.Sprint(candles[period][ticker][i].open)}
				str = append(str, fmt.Sprint(candles[period][ticker][i].high), fmt.Sprint(candles[period][ticker][i].low), fmt.Sprint(candles[period][ticker][i].close))
				strings[period][ticker][i] = str
			}
		}
	}
	for period := range candles{
		maxLength := maxSliceLength(candles[period])
		for i := 0; i < maxLength; i++ {
			for ticker := range candles[period] {
				if candles[period][ticker][i].opened {
					err := writers[period].Write(strings[period][ticker][i])
					if err != nil {
						fmt.Println("can not write candle : ", err)
						panic(err)
					}
				}
			}
		}
	}
	for i := range candles {
		writers[i].Flush()
	}
}

func elementNumber(candles []Candle, t time.Time) int {
	for i := range candles {
		if t.Equal(candles[i].timeOpen) || t.After(candles[i].timeOpen) && t.Before(candles[i].timeClose) {
			return i
		}
	}
	return -1
}

func (c *Candle) setOpen(price float64) {
	c.opened = true
	c.open = price
	c.high = price
	c.low = price
	c.close = price
}

func (c *Candle) setHighLowClose(price float64) {
	if c.high < price {
		c.high = price
	}
	if c.low > price {
		c.low = price
	}
	c.close = price
}

//candle[5, 30, 240]["SBER", "AAPL", "AMZN"][slice]
func checkPrice(candles [3][3][]Candle, line []string) [3][3][]Candle {
	var ticker int8
	switch line[0] {
	case "SBER":
		ticker = 0
	case "AAPL":
		ticker = 1
	case "AMZN":
		ticker = 2
	}
	t, err := time.Parse(timeLayout, line[3])
	if err != nil {
		fmt.Println("can not parse time from trades.csv(checkPrice) : ", err)
		panic(err)
	}
	price, err := strconv.ParseFloat(line[1], 64)
	if err != nil {
		fmt.Println("can not parse float from string : ", err)
		panic(err)
	}
	for period := 0; period < 3; period++ {
		number := elementNumber(candles[period][ticker], t)
		if !candles[period][ticker][number].opened {
			candles[period][ticker][number].setOpen(price)
		} else {
			candles[period][ticker][number].setHighLowClose(price)
		}
	}
	return candles
}

func main() {

	path, _ := filepath.Abs("..")
	file, err := os.Open(filepath.Join(path, "hw1", "data", "trades.csv"))
	if err != nil {
		fmt.Println("can not open trades.csv : ", err)
		panic(err)
	}

	files := [3]*os.File{}
	files[0], _ = os.Create(filepath.Join(path, "hw1","data", "candles_5m.csv"))
	files[1], _ = os.Create(filepath.Join(path, "hw1","data", "candles_30m.csv"))
	files[2], _ = os.Create(filepath.Join(path, "hw1","data", "candles_240m.csv"))
	reader := csv.NewReader(file)
	writers := [3]csv.Writer{}
	for i := 0; i < 3; i++ {
		writers[i] = *csv.NewWriter(files[i])
	}

	openTime, _ := time.Parse(timeLayout, "2019-01-30 07:00:00")
	closeTime, _ := time.Parse(timeLayout, "2019-01-31 00:00:00")

	candles := initSlices(openTime, closeTime)

	nextDay := false
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		}
		t, err := time.Parse(timeLayout, line[3])
		if err != nil {
			fmt.Println("can not parse time from trades.csv : ", err)
			panic(err)
		}
		//There is no problems with equality with open and close time
		/*if t.Equal(openTime) || t.Equal(closeTime) {
			fmt.Println(t)
		}*/
		if t.After(openTime) && t.Before(closeTime) {
			nextDay = true
			candles = checkPrice(candles, line)
		} else if t.After(openTime) && t.After(closeTime) && nextDay {
			nextDay = false
			openTime = openTime.Add(time.Hour * 24)
			closeTime = closeTime.Add(time.Hour * 24)
			writeSlices(candles, writers)
			candles = initSlices(openTime, closeTime)
		}
	}

	writeSlices(candles, writers)

	err = file.Close()
	if err != nil {
		fmt.Println("file : ", err)
		panic(err)
	}
	for i := 0; i < 3; i++ {
		err = files[i].Close()
		if err != nil {
			fmt.Println("writer ", strconv.Itoa(i), " : ", err)
			panic(err)
		}
	}
}
